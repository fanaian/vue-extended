let userAgent = navigator.userAgent || navigator.vendor || window.opera;
export function checkPlatformDevice() {
  // eslint-disable-next-line no-undef
  try {
    if (cordova.device.platform.toUpperCase() === 'ANDROID') {
      return "mountedAndroid";
    }
    if (cordova.device.platform.toUpperCase() === 'IOS'){
      return "mountedIos";
    }
  } catch (e) {}
  if (userAgent.indexOf(' electron/') > -1) {
    return "mountedElectron";
  } else {
    return false
  }
}
export function checkDeviceType() {
  // eslint-disable-next-line no-undef
  try {
    if (cordova) {
      return "mountedMobile";
    }
  } catch (e) {
    if (!(userAgent.indexOf(' electron/') > -1)) {
      return "mountedWeb";
    } else if ((userAgent.indexOf(' electron/') > -1)) {
      return "mountedDesktop"
    } else {
      return false
    }
  }
}
